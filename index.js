let http = require("http");

const port = 4000;

http.createServer((request,response) => {

	if(request.url == "/profile" && request.method == "GET"){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end("Welcome to your profile!");
	}

	else if(request.url == "/" && request.method == "GET"){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end("Welcome to booking system!");
	}

	else if(request.url == "/courses" && request.method == "GET"){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end("Here's our courses available");
	}

	else if(request.url == "/addCourse" && request.method == "POST"){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end("Add course to our resources");
	}

}).listen(4000);

console.log("Server running at localhost: 4000");